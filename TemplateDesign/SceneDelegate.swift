//
//  SceneDelegate.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/20/21.
//

import UIKit
import IQKeyboardManagerSwift
import SideMenu

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var navController:UINavigationController!

    var window: UIWindow? {
      didSet {
        window?.overrideUserInterfaceStyle = .light
      }
    }


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        
        Thread.sleep(forTimeInterval: 3.0)
        
        IQKeyboardManager.shared.enable = true

        NSSetUncaughtExceptionHandler { (exception) in
           let stack = exception.callStackReturnAddresses
           print("Stack trace: \(stack)")

            }
        
        guard let _ = (scene as? UIWindowScene) else { return }
        
//        let token = UserDefaults.standard.object(forKey: "Usertype")
//            if token == nil {
//
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    // make sure to set storyboard id in storyboard for these VC
//                let startingVC =  storyboard.instantiateViewController(withIdentifier: "LoginPage");
//                    navController = SideMenuNavigationController(rootViewController: startingVC)
//                    self.window!.rootViewController = navController
//
//
//            }else{
//
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    // make sure to set storyboard id in storyboard for these VC
//                let startingVC =  storyboard.instantiateViewController(withIdentifier: "HomePage");
//                    navController = SideMenuNavigationController(rootViewController: startingVC)
//                    self.window!.rootViewController = navController
//
//
//            }
//
      
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

