//
//  ProfilePage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/29/21.
//

import UIKit
import Alamofire
import SDWebImage
import SideMenu

class ProfilePage: UIViewController {
    
    @IBOutlet weak var cardview: UIScrollView!
    @IBOutlet weak var mmTagTf: UITextField!
    @IBOutlet weak var usernameTf: UITextField!
    @IBOutlet weak var passwordTf: UITextField!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var emailTf: UITextField!
    @IBOutlet weak var codeLbl: UILabel!
    @IBOutlet weak var pagetitle: UILabel!
    @IBOutlet weak var savebuttn: UIButton!

    var useranmesave = String()
    var profile = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setuppage()
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        getprofileData()
        
    }
    

    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)

    }
    
    @IBAction func changepasswordBtnClicked(_ sender: UIButton) {
        
        let chpass = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordPage") as! ChangePasswordPage
        
        chpass.usernameget = useranmesave
        
        self.navigationController?.pushViewController(chpass, animated: true)
        
        
    }
    
    @IBAction func profilesubmit(_ sender: UIButton) {
           
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        updateprofileApi()

        
       }
    
    
    func setuppage()  {
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
  
    }
    
    
    func getprofileData()  {
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalClass.DevlopmentApi + "customer/?customer_id=\(customerId)"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "action": "customer"
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                  
                                    
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                  //  print("profile data - \(dict1)")
                                  
                                    let profileAddData:NSArray = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    if profileAddData.count == 0 {
                                     
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                        
                                    }else{
                                    
                                    
                                    let firstobj:NSDictionary  = profileAddData.object(at: 0) as! NSDictionary
                                    
                                    let customerdict:NSDictionary  = firstobj["customer"] as! NSDictionary
                                   
                                    print("profile data - \(firstobj)")
                                    
                                    mmTagTf.text! = firstobj["salutation"]as! String
                                    usernameTf.text! = customerdict["first_name"]as! String
                                    emailTf.text! = customerdict["email"]as! String
                                    passwordTf.text! = customerdict["last_name"]as! String
                                    useranmesave = customerdict["username"]as! String
                                    let number = firstobj["phone_number"]as! String
                                
                                    if number.contains("+91") {
                                        
                                        let result1 = String(number.dropFirst(3))
                                        
                                        mobileTF.text = result1
                                        codeLbl.text = "+91"

                                        
                                    }else if number.contains("+44") {
                                        let result1 = String(number.dropFirst(3))
                                        
                                        mobileTF.text = result1
                                        codeLbl.text = "+44"
                                        
                                    }else{
                                        
                                        let result1 = String(number.dropFirst(2))
                                        
                                        mobileTF.text = result1
                                        codeLbl.text = "+1"
                                    }
                                    
                                    }
                                    ERProgressHud.sharedInstance.hide()

                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                           }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    
    func updateprofileApi() {
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let numbercomb = codeLbl.text! + mobileTF.text!
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalClass.DevlopmentApi + "user/\(customerId)/"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "action": "user"
            ]

        AF.request(urlString, method: .put, parameters: ["first_name": usernameTf.text!, "last_name":passwordTf.text!,"username":useranmesave,"email":emailTf.text!.lowercased(),"phone_number":numbercomb],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                    print("profile data - \(dict1)")
                                  
//                                    let profileAddData:NSArray = (dict1.value(forKey:"results")as! NSArray)
//
//                                    let firstobj:NSDictionary  = profileAddData.object(at: 0) as! NSDictionary
//
//                                    let customerdict:NSDictionary  = firstobj["customer"] as! NSDictionary
//
//                                    print("profile data - \(firstobj)")
//
//                                    mmTagTf.text! = firstobj["salutation"]as! String
//                                    usernameTf.text! = customerdict["first_name"]as! String
//                                    emailTf.text! = customerdict["email"]as! String
//                                    passwordTf.text! = customerdict["last_name"]as! String
//                                    useranmesave = customerdict["username"]as! String
//                                    let number = firstobj["phone_number"]as! String
//
//                                    if number.contains("+91") {
//
//                                        let result1 = String(number.dropFirst(3))
//
//                                        mobileTF.text = result1
//                                        codeLbl.text = "+91"
//
//
//                                    }else{
//
//                                        let result1 = String(number.dropFirst(2))
//
//                                        mobileTF.text = result1
//                                        codeLbl.text = "+1"
//                                    }
//
                                    self.updatenumberApi()
                                  //  showSimpleAlert(messagess: "Profile updated successfully")
                                    
                                  //  self.dissmiss()
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                           }else if response.response?.statusCode == 400{
                            
                            ERProgressHud.sharedInstance.hide()

                            let dict :NSDictionary = response.value! as! NSDictionary
                            
                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
               }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func updatenumberApi() {
        
        let now = Date()

            let formatter = DateFormatter()

            formatter.timeZone = TimeZone.current

            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS" ///2020-10-19 12:05:10.625

            let dateString = formatter.string(from: now)
        
        print(dateString)
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let numbercomb = codeLbl.text! + mobileTF.text!
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalClass.DevlopmentApi + "customer/\(customerId)/"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "action": "customer"
            ]

        AF.request(urlString, method: .put, parameters: ["first_name": usernameTf.text!, "last_name":passwordTf.text!,"username":useranmesave,"email":emailTf.text!,"phone_number":numbercomb,"last_access":dateString,"salutation":mmTagTf.text!],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                    print("profile data - \(dict1)")
                                  
//                                    let profileAddData:NSArray = (dict1.value(forKey:"results")as! NSArray)
//
//                                    let firstobj:NSDictionary  = profileAddData.object(at: 0) as! NSDictionary
//
//                                    let customerdict:NSDictionary  = firstobj["customer"] as! NSDictionary
//
//                                    print("profile data - \(firstobj)")
//
//                                    mmTagTf.text! = firstobj["salutation"]as! String
//                                    usernameTf.text! = customerdict["first_name"]as! String
//                                    emailTf.text! = customerdict["email"]as! String
//                                    passwordTf.text! = customerdict["last_name"]as! String
//                                    useranmesave = customerdict["username"]as! String
//                                    let number = firstobj["phone_number"]as! String
//
//                                    if number.contains("+91") {
//
//                                        let result1 = String(number.dropFirst(3))
//
//                                        mobileTF.text = result1
//                                        codeLbl.text = "+91"
//
//
//                                    }else{
//
//                                        let result1 = String(number.dropFirst(2))
//
//                                        mobileTF.text = result1
//                                        codeLbl.text = "+1"
//                                    }
//
                                  
                                    showSimpleAlert(messagess: "Profile updated successfully")
                                    
                                    ERProgressHud.sharedInstance.hide()

                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                           }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
        
        func SessionAlert() {
            let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

          
            alert.addAction(UIAlertAction(title: "OK",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            ERProgressHud.sharedInstance.hide()
                                            //Sign out action
                                          
                                            UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                            UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                            UserDefaults.standard.removeObject(forKey: "custToken")
                                            UserDefaults.standard.removeObject(forKey: "custId")
                                        UserDefaults.standard.removeObject(forKey: "Usertype")
                                        UserDefaults.standard.synchronize()
                                            
                                            let LoginPage = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! LoginPage
                                            self.navigationController?.pushViewController(LoginPage, animated: true)
                                            
                                          
                                          }))
            self.present(alert, animated: true, completion: nil)
            alert.view.tintColor = UIColor.black
        }
        
        
    

}
