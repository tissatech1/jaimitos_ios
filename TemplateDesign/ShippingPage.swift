//
//  ShippingPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 5/10/21.
//

import UIKit
import Alamofire
import SDWebImage
import SideMenu

class ShippingPage: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var storePicBtn: UIButton!
    @IBOutlet weak var homepicBtn: UIButton!
    @IBOutlet weak var shippingreenView: UIView!
    @IBOutlet weak var greenLbl: UILabel!
    @IBOutlet weak var noteLbl: UILabel!
    @IBOutlet weak var notefullView: UIView!
    @IBOutlet weak var exixtingaddBtn: UIButton!
    @IBOutlet weak var existingTitleLbl: UILabel!
    @IBOutlet weak var exixtingAddLbl: UILabel!
    @IBOutlet weak var notehalfView: UIView!
    @IBOutlet weak var fullnameLbl: UILabel!
    @IBOutlet weak var fullnameView: UIView!
    @IBOutlet weak var companynameLbl: UILabel!
    @IBOutlet weak var companyLblView: UIView!
    @IBOutlet weak var addLineLbl: UILabel!
    @IBOutlet weak var addLineView: UIView!
    @IBOutlet weak var housenumLbl: UILabel!
    @IBOutlet weak var housenumView: UIView!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var zipLbl: UILabel!
    @IBOutlet weak var zipView: UIView!
    @IBOutlet weak var countrylbl: UILabel!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var pagescrollView: UIScrollView!
    @IBOutlet weak var fullnameTopCont: NSLayoutConstraint!
    @IBOutlet weak var usebillingNoteLbl: UILabel!
    @IBOutlet weak var usebillingtickBtn: UIButton!
    @IBOutlet weak var fullnameTf: UITextField!
    @IBOutlet weak var companynameTf: UITextField!
    @IBOutlet weak var addresslineTf: UITextField!
    @IBOutlet weak var housenumberTf: UITextField!
    @IBOutlet weak var cityTf: UITextField!
    @IBOutlet weak var zipTf: UITextField!
    @IBOutlet weak var countryTf: UITextField!
    @IBOutlet weak var stateTf: UITextField!
    @IBOutlet weak var shippingNextBtn: UIButton!
    @IBOutlet weak var shippingmethodfullview: UIView!
    @IBOutlet weak var storeselectLbl: UILabel!
    @IBOutlet weak var homeselctLbl: UILabel!
    @IBOutlet weak var blurview: UIView!
    @IBOutlet weak var stateTable: UITableView!
    @IBOutlet weak var methodtable: UITableView!
    @IBOutlet weak var methodtableheight: NSLayoutConstraint!
    
    var selectedIndex:NSIndexPath?
    var methodchoose  : [String] = []
    var StateDict = NSMutableArray()
    var shippingAddData = NSArray()
    var billingaddressData = NSArray()
    var billing = NSString()
    var shippingMethod = NSArray()
    var shipping = NSString()
    var shippingId = NSString()
    var passshippingData = NSDictionary()
    var passbillingdata = NSDictionary()
    var billId = NSString()
    
    var nameadd = NSString()
    var addressadd = NSString()
    var hounseadd = NSString()
    var cityadd = NSString()
    var stateadd = NSString()
    var countryadd = NSString()
    var sipadd = NSString()
    var companynameadd = NSString()
    
    var useexistadd = NSString()
    var storeradioclick = NSString()
    var billingtickStr = NSString()
    var anyselectStr = NSString()
    
    var userexistbilltick = NSString()

    override func viewDidLoad() {
        super.viewDidLoad()
        blurview.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        loadshippingMethod()
        loadState()
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        setup()
        loadBillingAddress()
        
        billingtickStr = "no"
        shippingNextBtn.layer.cornerRadius = 6
        self.viewhide()
        self.anyselectStr = "noselect"
        
        methodtable.register(UINib(nibName: "methodcell", bundle: nil), forCellReuseIdentifier: "cell")
        
        
    }
    
    @IBAction func statebutnClicked(_ sender: Any) {
        blurview.isHidden = false
        
    }
    
    @IBAction func blurcancelClicked(_ sender: Any) {
        
        blurview.isHidden = true
        
    }
    
    func loadState(){
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      
        let urlString = GlobalClass.DevlopmentApi + "tax/?country_code=US"
      
       // print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "billing"
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                    
                                    let dict1 :NSDictionary = response.value! as! NSDictionary
                                       
                                       print("billing address - \(dict1)")
                                     
                                    
                                    
                                    let arraysh: NSArray  = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    if arraysh.count == 0 {
                                       
                                        self.StateDict = []
                                        
                                        stateTable.reloadData()
                                        
                                    }else{
                                        
                                        self.StateDict.addObjects(from:arraysh as! [Any])
                                        
                                     
                                        
                                       stateTable.reloadData()

                                    }
                                    
                                
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 404{
                                        
                                       
                                        
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                        
                                    }
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                                    self.showSimpleAlert(messagess:"Slow Internet Detected")
                                            
                                        }else{
                                        
                                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                        }

                                           print(error)
                                    }
            }
            
      
            
        }
   
    
    
    func setup() {
        
      //  setupToolbar()
        self.exixtingaddBtn.tag = 0
        usebillingtickBtn.tag = 0
        
        notehalfView.layer.borderWidth = 1
        notehalfView.layer.cornerRadius = 6
        notehalfView.layer.borderColor = UIColor.lightGray.cgColor
        
        fullnameView.layer.borderWidth = 1
        fullnameView.layer.cornerRadius = 6
        fullnameView.layer.borderColor = UIColor.lightGray.cgColor
        
//        companyLblView.layer.borderWidth = 1
//        companyLblView.layer.cornerRadius = 6
//        companyLblView.layer.borderColor = UIColor(rgb: 0x199A48).cgColor
        
        addLineView.layer.borderWidth = 1
        addLineView.layer.cornerRadius = 6
        addLineView.layer.borderColor = UIColor.lightGray.cgColor
        
        housenumView.layer.borderWidth = 1
        housenumView.layer.cornerRadius = 6
        housenumView.layer.borderColor = UIColor.lightGray.cgColor
        
        cityView.layer.borderWidth = 1
        cityView.layer.cornerRadius = 6
        cityView.layer.borderColor = UIColor.lightGray.cgColor
        
        zipView.layer.borderWidth = 1
        zipView.layer.cornerRadius = 6
        zipView.layer.borderColor = UIColor.lightGray.cgColor
        
        countryView.layer.borderWidth = 1
        countryView.layer.cornerRadius = 6
        countryView.layer.borderColor = UIColor.lightGray.cgColor
        
        stateView.layer.borderWidth = 1
        stateView.layer.cornerRadius = 6
        stateView.layer.borderColor = UIColor.lightGray.cgColor
        
    }
   
    func loadshippingaddress(){
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalClass.DevlopmentApi + "shipping/?customer_id=\(customerId)"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "shipping"
            ]

             AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                  
                                    
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                    print("Shipping address - \(dict1)")
                                  
                                self.shippingAddData = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    
                                      if self.shippingAddData.count == 0 {
                                       
                                        ERProgressHud.sharedInstance.hide();
                                        
                                        self.shipping = "no"
                                        self.useexistadd = "no"
                                        self.storeradioclick = "no"
                                        
                                      
                                     
                                 }else{
                                    

                                       
                                    self.shipping = "yes"
                                    
                                    self.useexistadd = "yes"

                                   let firstobj:NSDictionary  = self.shippingAddData.object(at: 0) as! NSDictionary
                                    
                                    self.passshippingData = firstobj

                                    let defaults = UserDefaults.standard
                                    defaults.set(self.passbillingdata, forKey: "shippingaddressDICT")
                                    
                                   let shipId = firstobj["id"] as! Int

                                    self.shippingId = String(shipId) as NSString
                                    ERProgressHud.sharedInstance.hide()
                                
                                 }
                                     
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()
                                    
                                        self.SessionAlert()
                                        
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
        
   

    
    func loadshippingMethod(){
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let storeId = defaults.object(forKey: "clickedStoreId")as? String
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
        let urlString = GlobalClass.DevlopmentApi + "shipping-method/?status=ACTIVE&store_id=\(storeId ?? "0")&restaurant_id=\(GlobalClass.restaurantGlobalid)"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

             AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                  
                                    
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                    print("shipping Method - \(dict1)")
                                  
                                self.shippingMethod = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    print("shipping Method result - \(self.shippingMethod)")
                                    
                                      if self.shippingMethod.count == 0 {
                                       
                                        self.shippingMethod = []
                                        
                                      //  ERProgressHud.sharedInstance.hide()
                                        
                                        self.viewhide()
                                        self.shippingmethodfullview.isHidden = true
                                        self.storeselectLbl.isHidden = true
                                        self.homeselctLbl.isHidden = true
                                        
                                        self.showSimpleAlert(messagess: "No shipping method available")
                                        
                                     
                                 }else{
                                       
                                    for elements in self.shippingMethod as! [[String:Any]] {
                                      
                                        let settag = "NO"
                                        self.methodchoose.append(settag)
                                    }
                                    
                                    print(self.methodchoose)
                                    self.methodtableheight.constant = CGFloat(self.shippingMethod.count * 50)
                                    self.methodtable.reloadData()
                                    
                                    
                                  //  ERProgressHud.sharedInstance.hide()
                                
                                 }
                                     
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()
                                    
                                        self.SessionAlert()
                                        
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: messagess, message: nil,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        //Sign out action
                                        ERProgressHud.sharedInstance.hide()
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let home = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(home, animated: true)
                                        
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    

    func viewshow() {
        
      //  shippingreenView.isHidden = false
      //  greenLbl.isHidden = false
        fullnameLbl.isHidden = false
        fullnameView.isHidden = false
      //  companynameLbl.isHidden = false
      //  companyLblView.isHidden = false
        addLineLbl.isHidden = false
        addLineView.isHidden = false
        housenumLbl.isHidden = false
        housenumView.isHidden = false
        cityLbl.isHidden = false
        cityView.isHidden = false
        zipLbl.isHidden = false
        zipView.isHidden = false
        countrylbl.isHidden = false
        countryView.isHidden = false
        stateLbl.isHidden = false
        stateView.isHidden = false
        usebillingNoteLbl.isHidden = false
        usebillingtickBtn.isHidden = false
        fullnameTf.isHidden = false
     //   companynameTf.isHidden = false
        addresslineTf.isHidden = false
        housenumberTf.isHidden = false
        cityTf.isHidden = false
        zipTf.isHidden = false
        countryTf.isHidden = false
        stateTf.isHidden = false
      
        if shipping == "no" {
            
            self.notefullView.isHidden = true
            self.notehalfView.isHidden = true
            self.existingTitleLbl.isHidden = true
            self.exixtingaddBtn.isHidden = true
            self.exixtingAddLbl.isHidden = true
            self.noteLbl.isHidden = true
            self.fullnameTopCont.constant = 12
            
        }else{
            
            self.notefullView.isHidden = false
            self.notehalfView.isHidden = false
            self.existingTitleLbl.isHidden = false
            self.exixtingaddBtn.isHidden = false
            self.exixtingAddLbl.isHidden = false
            self.noteLbl.isHidden = false
            self.fullnameTopCont.constant = 169
            
        }
        
        
        
        
    }
    
    func viewhide() {
        
        //shippingreenView.isHidden = true
      //  greenLbl.isHidden = true
        fullnameLbl.isHidden = true
        fullnameView.isHidden = true
       // companynameLbl.isHidden = true
       // companyLblView.isHidden = true
        addLineLbl.isHidden = true
        addLineView.isHidden = true
        housenumLbl.isHidden = true
        housenumView.isHidden = true
        cityLbl.isHidden = true
        cityView.isHidden = true
        zipLbl.isHidden = true
        zipView.isHidden = true
        countrylbl.isHidden = true
        countryView.isHidden = true
        stateLbl.isHidden = true
        stateView.isHidden = true
        usebillingNoteLbl.isHidden = true
        usebillingtickBtn.isHidden = true
        fullnameTf.isHidden = true
       // companynameTf.isHidden = true
        addresslineTf.isHidden = true
        housenumberTf.isHidden = true
        cityTf.isHidden = true
        zipTf.isHidden = true
        countryTf.isHidden = true
        stateTf.isHidden = true
        
        if shipping == "no" {
            
            self.notefullView.isHidden = true
            self.notehalfView.isHidden = true
            self.existingTitleLbl.isHidden = true
            self.exixtingaddBtn.isHidden = true
            self.exixtingAddLbl.isHidden = true
            self.noteLbl.isHidden = true
            self.fullnameTopCont.constant = 12
            
        }else{
            
            self.notefullView.isHidden = true
            self.notehalfView.isHidden = true
            self.existingTitleLbl.isHidden = true
            self.exixtingaddBtn.isHidden = true
            self.exixtingAddLbl.isHidden = true
            self.noteLbl.isHidden = true
            self.fullnameTopCont.constant = 169
            
        }
        
        
        
    }
    
    
    
    
    //MARK: Button Actions
    
    
    @IBAction func shippingpagebackClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)

    }
    
   
   
//    @IBAction func storepickerBtnclicked(_ sender: Any) {
//
//        let imagehome = UIImage(named: "dot.png") as UIImage?
//        self.storePicBtn.setBackgroundImage(imagehome, for: .normal)
//
//        let image = UIImage(named: "untickredio.png") as UIImage?
//        self.homepicBtn.setBackgroundImage(image, for: .normal)
//
//        self.viewhide()
//        pagescrollView.isScrollEnabled = false
//        self.storeradioclick = "yes"
//        self.anyselectStr = "yes"
//
//        let defaults = UserDefaults.standard
//
//        let dictObj = self.shippingMethod[0] as! NSDictionary
//
//        let status = "PICKUP"
//        let statusid = dictObj["id"]
//
//        defaults.set(status, forKey: "clickedShippingMethod")
//        defaults.set(statusid, forKey: "clickedShippingMethodId")
//
//    }
    
//    @IBAction func homeDelivBtnClicked(_ sender: Any) {
//
//        let defaults = UserDefaults.standard
//
//        let amount = defaults.object(forKey: "totalcartPrice")as? String
//
//        if amount == nil {
//
//            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
//
//        }else{
//
//
//        let dictObj = self.shippingMethod[0] as! NSDictionary
//
//        let status = "SHIPMENT"
//        let statusid = dictObj["id"]
//
//        defaults.set(status, forKey: "clickedShippingMethod")
//        defaults.set(statusid, forKey: "clickedShippingMethodId")
//
//        let imagestore = UIImage(named: "untickredio.png") as UIImage?
//        self.storePicBtn.setBackgroundImage(imagestore, for: .normal)
//
//        let image = UIImage(named: "dot.png") as UIImage?
//        self.homepicBtn.setBackgroundImage(image, for: .normal)
//
//        self.viewshow()
//        pagescrollView.isScrollEnabled = true
//
//        self.storeradioclick = "no"
//
//        self.anyselectStr = "yes"
//
//        if self.shippingAddData.count == 0 {
//
//
//        }else{
//
//            self.useexistadd = "yes"
//
//        let firstobj:NSDictionary  = self.shippingAddData.object(at: 0) as! NSDictionary
//
//        self.nameadd = firstobj["name"] as! NSString
//        self.addressadd = firstobj["address"]as! NSString
//        self.hounseadd = firstobj["house_number"]as! NSString
//        self.cityadd = firstobj["city"]as! NSString
//        self.stateadd = firstobj["state"]as! NSString
//        self.countryadd = firstobj["country"]as! NSString
//        self.sipadd = firstobj["zip"]as! NSString
//        self.companynameadd = firstobj["company_name"]as! NSString
//
//        let shipId = firstobj["id"] as! Int
//
//        self.shippingId = String(shipId) as NSString
//
//        self.exixtingAddLbl.text = "\(self.nameadd),\n"+"\(self.addressadd),"+"\(self.hounseadd)\n"+"\(self.cityadd),"+"\(self.stateadd),"+"\(self.countryadd),\n"+"\(self.sipadd)."
//
//
//        let imagenew = UIImage(named:"right.png") as UIImage?
//        self.exixtingaddBtn.setBackgroundImage(imagenew, for: .normal)
//        self.exixtingaddBtn.tag = 1
//
//        self.fullnameTf.isUserInteractionEnabled = false
//      //  self.companynameTf.isUserInteractionEnabled = false
//        self.addresslineTf.isUserInteractionEnabled = false
//        self.housenumberTf.isUserInteractionEnabled = false
//        self.cityTf.isUserInteractionEnabled = false
//        self.zipTf.isUserInteractionEnabled = false
//        self.countryTf.isUserInteractionEnabled = false
//        self.stateTf.isUserInteractionEnabled = false
//
//        }
//      //  }
//        }
//    }
    
    
    @IBAction func existingaddBtnClicked(_ sender: Any) {
        
        if exixtingaddBtn.tag == 0 {
           
            
            let image = UIImage(named: "right.png") as UIImage?
            self.exixtingaddBtn.setBackgroundImage(image, for: .normal)
            
            self.fullnameTf.isUserInteractionEnabled = false
         //   self.companynameTf.isUserInteractionEnabled = false
            self.addresslineTf.isUserInteractionEnabled = false
            self.housenumberTf.isUserInteractionEnabled = false
            self.cityTf.isUserInteractionEnabled = false
            self.zipTf.isUserInteractionEnabled = false
            self.countryTf.isUserInteractionEnabled = false
            self.stateTf.isUserInteractionEnabled = false
            
            exixtingaddBtn.tag = 1
            self.useexistadd = "yes"
            
        }else{
          
            let image = UIImage(named: "blank-square.png") as UIImage?
            self.exixtingaddBtn.setBackgroundImage(image, for: .normal)
            
            self.fullnameTf.isUserInteractionEnabled = true
         //   self.companynameTf.isUserInteractionEnabled = true
            self.addresslineTf.isUserInteractionEnabled = true
            self.housenumberTf.isUserInteractionEnabled = true
            self.cityTf.isUserInteractionEnabled = true
            self.zipTf.isUserInteractionEnabled = true
            self.countryTf.isUserInteractionEnabled = true
            self.stateTf.isUserInteractionEnabled = true
            
            exixtingaddBtn.tag = 0
            self.useexistadd = "no"
            
        }
        
    }
    
    @IBAction func billingtickClicked(_ sender: Any) {
        
        if usebillingtickBtn.tag == 0 {
           
            
            let image = UIImage(named: "right.png") as UIImage?
            self.usebillingtickBtn.setBackgroundImage(image, for: .normal)
            usebillingtickBtn.tag = 1
            billingtickStr = "yes"
        }else{
          
            let image = UIImage(named: "blank-square.png") as UIImage?
            self.usebillingtickBtn.setBackgroundImage(image, for: .normal)
            usebillingtickBtn.tag = 0
            billingtickStr = "no"
            
        }
        
        
    }
    
    
    @IBAction func shippingnextBtnClicked(_ sender: Any) {
        
       
        
        if self.anyselectStr == "noselect" {
            
            self.showSimpleAlert(messagess: "Select shipping method")
            
        }else{
        
            
        if self.storeradioclick == "yes" {
            
            let csrt = self.storyboard?.instantiateViewController(withIdentifier: "BillPage") as! BillPage
            self.navigationController?.pushViewController(csrt, animated: true)
            
        }else{
        
        
        
        if shipping == "no" {
        

            print("fulladd=\(fullnameTf.text ?? "nodata")")
            
            if fullnameTf.text == "" || fullnameTf.text == nil {
                self.showSimpleAlert(messagess: "Enter full name")
            }else if addresslineTf.text == nil || addresslineTf.text == "" {
                self.showSimpleAlert(messagess: "Enter address line")
            }else if cityTf.text == nil || cityTf.text == "" {
                self.showSimpleAlert(messagess: "Enter city")
            }else if zipTf.text == nil || zipTf.text == "" {
                self.showSimpleAlert(messagess: "Enter zip code")
            }else{
            
                
                self.dataset()
                
            }
            
            
        }else{
            
            if self.useexistadd == "no" {
                
                print("fulladd=\(fullnameTf.text ?? "nodata")")
                

                    if fullnameTf.text == "" || fullnameTf.text == nil {
                        self.showSimpleAlert(messagess: "Enter full name")
                    }else if addresslineTf.text == nil || addresslineTf.text == "" {
                        self.showSimpleAlert(messagess: "Enter address line")
                    }else if cityTf.text == nil || cityTf.text == "" {
                        self.showSimpleAlert(messagess: "Enter city")
                    }else if zipTf.text == nil || zipTf.text == "" {
                        self.showSimpleAlert(messagess: "Enter zip code")
                    }else{

                    
                    
                    if self.billingtickStr == "no" {
                        userexistbilltick = "no"
                    }else{
                        userexistbilltick = "yes"
                        if self.billing == "no" {
                            
                            ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                          addBillingAddress()
                            
                        }else{
                            
                            ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                            updateBillingAddress()
                        }
                        
                    }
                    
                    self.dataset()
                    
                }
                
                
            }else{
                
                if self.billingtickStr == "no" {
                    //self.dissmiss()
                    userexistbilltick = "no"

                    let csrt = self.storyboard?.instantiateViewController(withIdentifier: "BillPage") as! BillPage
                    self.navigationController?.pushViewController(csrt, animated: true)
                }else{
                   // self.dissmiss()
                    userexistbilltick = "yes"
                    
                    if self.billing == "no" {
                        
                        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                      addBillingAddress()
                        
                    }else{
                        
                        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                        updateBillingAddress()
                    }
                    
 
                    
                }

                
            }
            

            
           
        }
        
        }
        }
        
        
        let defaults = UserDefaults.standard
        defaults.set(passshippingData, forKey: "passShippingDataDict")
        defaults.set(shippingId, forKey: "passedShippingId")
        
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Tell the keyboard where to go on next / go button.
       
        textField.resignFirstResponder()
        

        return true
    }
    
    func dataset()  {
        
        if self.shipping == "no" {
            
            self.nameadd = fullnameTf.text! as NSString
            self.addressadd = addresslineTf.text! as NSString
            self.hounseadd = housenumberTf.text! as NSString
            self.cityadd = cityTf.text! as NSString
            self.stateadd = stateTf.text! as NSString
            if countryTf.text == "India" {
                self.countryadd = "IN"
            }else{
                
                self.countryadd = "US"
            }
            
            self.sipadd = zipTf.text! as NSString
            self.companynameadd = ""
            
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                        self.addShippingAddress()
            
        }else{
            
            if self.useexistadd == "no" {
                
                self.nameadd = fullnameTf.text! as NSString
                self.addressadd = addresslineTf.text! as NSString
                self.hounseadd = housenumberTf.text! as NSString
                self.cityadd = cityTf.text! as NSString
                self.stateadd = stateTf.text! as NSString
                if countryTf.text == "India" {
                    self.countryadd = "IN"
                }else{
                    
                    self.countryadd = "US"
                }
                
                self.sipadd = zipTf.text! as NSString
                self.companynameadd = ""
                
                ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                self.updateShippingAddress()
                
            }else{
                
                
            }
            
            
        }
        
        
    }
    
    
    
    
    func addShippingAddress()  {
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let urlString = GlobalClass.DevlopmentApi+"shipping/"
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "shipping"
            ]
        

        AF.request(urlString, method: .post, parameters: ["name":self.nameadd,"company_name":self.companynameadd,"address":self.addressadd,"house_number":self.hounseadd,"zip":self.sipadd,"city":self.cityadd,"country":self.countryadd,"state":self.stateadd,"customer_id":"\(customerId)","priority":1],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                             
                             let dict :NSDictionary = response.value! as! NSDictionary
                              print("add shipping responce - \(dict)")
                                
                                self.passshippingData = dict
                                
                                let defaults = UserDefaults.standard
                                defaults.set(self.passbillingdata, forKey: "shippingaddressDICT")
                                
                                if self.billingtickStr == "no" {
                                    
                                    ERProgressHud.sharedInstance.hide();                                     let csrt = self.storyboard?.instantiateViewController(withIdentifier: "BillPage") as! BillPage
                                    self.navigationController?.pushViewController(csrt, animated: true)
                                }else{
                                    ERProgressHud.sharedInstance.hide()
                                    if self.userexistbilltick == "yes" {
                                        
                                        
                                    }else{
                                    
                                        let csrt = self.storyboard?.instantiateViewController(withIdentifier: "PaymentPage") as! PaymentPage
                                        self.navigationController?.pushViewController(csrt, animated: true)
                                    }
                                    
                                    
                                    
                                }
                                
                                
                                
                            }else{
                                
                                if response.response?.statusCode == 401{
                                    
                                    ERProgressHud.sharedInstance.hide();                                    self.SessionAlert()
                                    
                                }else if response.response?.statusCode == 500{
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                                
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide();                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            
                            if error.localizedDescription == msg {
                                
                                self.showSimpleAlert(messagess:"No internet connection")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }

        
        
    }
    
    func updateShippingAddress()  {
        
        let  idd = Int(shippingId as String)
        
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let urlString = GlobalClass.DevlopmentApi+"shipping/\(idd ?? 0)/"
        
        print("update query - \(urlString)")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "shipping"
            ]
        

        AF.request(urlString, method: .put, parameters: ["name":self.nameadd,"company_name":self.companynameadd,"address":self.addressadd,"house_number":self.hounseadd,"zip":self.sipadd,"city":self.cityadd,"country":self.countryadd,"state":self.stateadd,"customer_id":"\(customerId)","priority":1],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 200{
                             
                             let dict :NSDictionary = response.value! as! NSDictionary
                              print("update shipping responce - \(dict)")
                                
                                ERProgressHud.sharedInstance.hide()

                                self.passshippingData = dict
                                
                                let defaults = UserDefaults.standard
                                defaults.set(self.passbillingdata, forKey: "shippingaddressDICT")
                                
                                if self.billingtickStr == "no" {
                                    ERProgressHud.sharedInstance.hide();                                     let csrt = self.storyboard?.instantiateViewController(withIdentifier: "BillPage") as! BillPage
                                    self.navigationController?.pushViewController(csrt, animated: true)
                                }else{
                                    ERProgressHud.sharedInstance.hide()
                                    if self.userexistbilltick == "yes" {
                                        
                                        
                                    }else{
                                    
                                        let csrt = self.storyboard?.instantiateViewController(withIdentifier: "PaymentPage") as! PaymentPage
                                        self.navigationController?.pushViewController(csrt, animated: true)
                                
                                    }
                                }
                                
                                
                                
                            }else{
                                
                                if response.response?.statusCode == 401{
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                    
                                }else if response.response?.statusCode == 500{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                                
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()
                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            
                            if error.localizedDescription == msg {
                                
                                self.showSimpleAlert(messagess:"No internet connection")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }

        
        
    }
    
    
    
    
    
    
    func loadBillingAddress(){
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)

        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
        let urlString = GlobalClass.DevlopmentApi + "billing/?customer_id=\(customerId)"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "billing"
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                  
                                    
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                    print("billing address - \(dict1)")
                                  
                                self.billingaddressData = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    
                                    
                                    if self.billingaddressData.count == 0 {
                                     
                                    //  self.dissmiss()
                                      self.billing = "no"
                                        
                                   
                               }else{

                                     
                                  self.billing = "yes"
                                
                                let firstobj:NSDictionary  = self.billingaddressData.object(at: 0) as! NSDictionary
                                
                              self.passbillingdata = firstobj
                              
                             
                              let shipId = firstobj["id"] as! Int

                               self.billId = String(shipId) as NSString
                      
                            
                              
                               }
                                    
                                    loadshippingaddress()
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 404{
                                        
                                       
                                        
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                        
                                    }
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    
    
    func addBillingAddress()  {
        
        
      if self.useexistadd == "no" {
        self.nameadd = fullnameTf.text! as NSString
        self.addressadd = addresslineTf.text! as NSString
        self.hounseadd = housenumberTf.text! as NSString
        self.cityadd = cityTf.text! as NSString
        self.stateadd = stateTf.text! as NSString
        if countryTf.text == "India" {
            self.countryadd = "IN"
        }else{
            
            self.countryadd = "US"
        }
        
        self.sipadd = zipTf.text! as NSString
        self.companynameadd = ""
        }else{
            
            
        }
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let urlString = GlobalClass.DevlopmentApi+"billing/"
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "billing"
            ]
        

        AF.request(urlString, method: .post, parameters: ["name":self.nameadd,"company_name":self.companynameadd,"address":self.addressadd,"house_number":self.hounseadd,"zip":self.sipadd,"city":self.cityadd,"country":self.countryadd,"state":self.stateadd,"customer_id":"\(customerId)","priority":1],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                             
                             let dict :NSDictionary = response.value! as! NSDictionary
                              print("add billing responce - \(dict)")
                                
                                
                                self.passbillingdata = dict

                                ERProgressHud.sharedInstance.hide()

                                if self.userexistbilltick == "yes"{
                                
                               
                                    let defaults = UserDefaults.standard
                                    defaults.set(self.passbillingdata, forKey: "billingaddressDICT")
                                    
                                    let csrt = self.storyboard?.instantiateViewController(withIdentifier: "PaymentPage") as! PaymentPage
                                    self.navigationController?.pushViewController(csrt, animated: true)
                                
                                }
                              
                                
                            }else{
                                
                                if response.response?.statusCode == 401{
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                    
                                }else if response.response?.statusCode == 500{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                    
                                }
                                
                            }
                            
                            break
                        case .failure(let error):
                            
                            ERProgressHud.sharedInstance.hide()
                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            
                            if error.localizedDescription == msg {
                                
                                self.showSimpleAlert(messagess:"No internet connection")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }

        
        
    }
    
    func updateBillingAddress()  {
        
        if self.useexistadd == "no" {
        self.nameadd = fullnameTf.text! as NSString
        self.addressadd = addresslineTf.text! as NSString
        self.hounseadd = housenumberTf.text! as NSString
        self.cityadd = cityTf.text! as NSString
        self.stateadd = stateTf.text! as NSString
        if countryTf.text == "India" {
            self.countryadd = "IN"
        }else{
            
            self.countryadd = "US"
        }
        
        self.sipadd = zipTf.text! as NSString
        self.companynameadd = ""
            
        }else{
            
            
        }
        
        let  idd = Int(billId as String)
        
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let urlString = GlobalClass.DevlopmentApi+"billing/\(idd ?? 0)/"
        
        print("update query - \(urlString)")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "billing"
            ]
        

        AF.request(urlString, method: .put, parameters: ["name":self.nameadd,"company_name":self.companynameadd,"address":self.addressadd,"house_number":self.hounseadd,"zip":self.sipadd,"city":self.cityadd,"country":self.countryadd,"state":self.stateadd,"customer_id":"\(customerId)","priority":1],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 200{
                             
                             let dict :NSDictionary = response.value! as! NSDictionary
                              print("update billing responce - \(dict)")
                                
                                ERProgressHud.sharedInstance.hide()

                                self.passbillingdata = dict

                               
                                ERProgressHud.sharedInstance.hide()

                                if self.userexistbilltick == "yes"{
                                
                                    let defaults = UserDefaults.standard
                                    defaults.set(self.passbillingdata, forKey: "billingaddressDICT")
                                    let csrt = self.storyboard?.instantiateViewController(withIdentifier: "PaymentPage") as! PaymentPage
                                    self.navigationController?.pushViewController(csrt, animated: true)
                                
                                }
                               
                                
                            }else{
                                
                                if response.response?.statusCode == 401{
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                    
                                }else  if response.response?.statusCode == 500{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                    
                                }
                                
                            }
                            
                            break
                        case .failure(let error):
                            
                            ERProgressHud.sharedInstance.hide()
                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            
                            if error.localizedDescription == msg {
                                
                                self.showSimpleAlert(messagess:"No internet connection")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }

        
        
    }
    

//MARK: - Table View Delegates And Datasource


// number of rows in table view
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if tableView == stateTable {
        return StateDict.count
    }else{
        return shippingMethod.count
    }
    
    
     

}

// create a cell for each table view row
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    if tableView == stateTable {
        
    let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)

     let dictObj = self.StateDict[indexPath.row] as! NSDictionary

    cell.textLabel!.text = dictObj["state"] as? String

    cell.textLabel?.textAlignment = .center

    return cell
        
    }else{
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! methodcell

      
        let dictObj = self.shippingMethod[indexPath.row] as! NSDictionary
        cell.methodname.text = dictObj["name"] as? String
        cell.outerview.layer.borderWidth = 0.5
        cell.outerview.layer.borderColor = UIColor.lightGray.cgColor
        
       
        return cell
    }
}


func tableView(_ tableView: UITableView,
               heightForRowAt indexPath: IndexPath) -> CGFloat{
      
    if tableView == stateTable {
        
        return 45
    }else{
        
        return 50
    }
}
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  
    if tableView == stateTable {
        
    let dictObj = self.StateDict[indexPath.row] as! NSDictionary
    blurview.isHidden = true
    stateTf.text = dictObj["state"] as? String
        
    }else{
       
        let currentCell = methodtable.cellForRow(at: indexPath) as! methodcell
        currentCell.cellBtn.image = UIImage(named: "dot.png")
        
        
        let dictObj = self.shippingMethod[indexPath.row] as! NSDictionary
        let methodid = dictObj["id"]as! NSNumber
        let methodStr = methodid.stringValue
        print(methodid)
        
        if methodStr == "30" {
            
            self.viewhide()
            pagescrollView.isScrollEnabled = false
            self.storeradioclick = "yes"
            self.anyselectStr = "yes"
            
            let defaults = UserDefaults.standard

            let dictObj = self.shippingMethod[indexPath.row] as! NSDictionary
            let status = "PICKUP"
            let statusid = ""
            defaults.set(status, forKey: "clickedShippingMethod")
            defaults.set(statusid, forKey: "clickedShippingMethodId")
            let statusidpay = dictObj["id"]
            defaults.set(statusidpay, forKey: "paymentpageShippingMethodId")
            
        }else{
            
            let defaults = UserDefaults.standard
            
            let amount = defaults.object(forKey: "totalcartPrice")as? String
            
            if amount == nil {
                
                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                
            }else{
            
           
            let dictObj = self.shippingMethod[indexPath.row] as! NSDictionary
               
            let status = "SHIPMENT"
                let statusidint = dictObj["id"]as! Int
                let statusid = String(statusidint)
                
            defaults.set(status, forKey: "clickedShippingMethod")
            defaults.set(statusid, forKey: "clickedShippingMethodId")
            
            self.viewshow()
            pagescrollView.isScrollEnabled = true
            
            self.storeradioclick = "no"
            
            self.anyselectStr = "yes"
            
            if self.shippingAddData.count == 0 {
                
                
            }else{
            
                self.useexistadd = "yes"
                
            let firstobj:NSDictionary  = self.shippingAddData.object(at: 0) as! NSDictionary
            
            self.nameadd = firstobj["name"] as! NSString
            self.addressadd = firstobj["address"]as! NSString
            self.hounseadd = firstobj["house_number"]as! NSString
            self.cityadd = firstobj["city"]as! NSString
            self.stateadd = firstobj["state"]as! NSString
            self.countryadd = firstobj["country"]as! NSString
            self.sipadd = firstobj["zip"]as! NSString
            self.companynameadd = firstobj["company_name"]as! NSString
            
            let shipId = firstobj["id"] as! Int
            
            self.shippingId = String(shipId) as NSString
            
            self.exixtingAddLbl.text = "\(self.nameadd),\n"+"\(self.addressadd),"+"\(self.hounseadd)\n"+"\(self.cityadd),"+"\(self.stateadd),"+"\(self.countryadd),\n"+"\(self.sipadd)."
            
            
            let imagenew = UIImage(named:"right.png") as UIImage?
            self.exixtingaddBtn.setBackgroundImage(imagenew, for: .normal)
            self.exixtingaddBtn.tag = 1
            
            self.fullnameTf.isUserInteractionEnabled = false
          //  self.companynameTf.isUserInteractionEnabled = false
            self.addresslineTf.isUserInteractionEnabled = false
            self.housenumberTf.isUserInteractionEnabled = false
            self.cityTf.isUserInteractionEnabled = false
            self.zipTf.isUserInteractionEnabled = false
            self.countryTf.isUserInteractionEnabled = false
            self.stateTf.isUserInteractionEnabled = false
                
            }
          //  }
            }
        }
    }
}
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == stateTable {
            
        }else{
             let currentCell = methodtable.cellForRow(at: indexPath) as! methodcell
               
            currentCell.cellBtn.image = UIImage(named: "untickredio.png")

        }
    }
    
    
   
    @IBAction func choosemethod(_ sender: UIButton) {
         

        
    }
    
    
    @IBAction func homeClikeched(_ sender: UIButton) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
        self.navigationController?.pushViewController(home, animated: true)
        
    }
    
    @IBAction func CartClicked(_ sender: UIButton) {
        
        let csrt = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
        self.navigationController?.pushViewController(csrt, animated: true)
        
    }
    
    @IBAction func orderClicked(_ sender: Any) {
        let home = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
        self.navigationController?.pushViewController(home, animated: true)
        
    }
    
    @IBAction func moreClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
    }
    
    


}

