//
//  MenuDetailPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/23/21.
//

import UIKit
import SideMenu
import SDWebImage
import Alamofire


class MenuDetailPage: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var cardview: UIView!
    @IBOutlet weak var homepagetabbar: UIView!
    @IBOutlet weak var ingredientstable: UITableView!
    @IBOutlet weak var productimage: UIImageView!
    @IBOutlet weak var tableheightconstant: NSLayoutConstraint!
    @IBOutlet weak var cartcountLbl: UILabel!
    @IBOutlet var productName: UILabel!
    @IBOutlet var productUnitPrice: UILabel!
    @IBOutlet var productSubtotalPrice: UILabel!
    @IBOutlet weak var ingredientlbl: UILabel!
    @IBOutlet weak var textbaseViewTf: UITextView!
    @IBOutlet weak var pricewithcounterLbl: UILabel!
    @IBOutlet weak var plusminusCount: UILabel!
    @IBOutlet weak var reqOneShowLbl: UILabel!

    
    var mustrequired = Bool()
    var passunitprice = String()
    var paasprodyctimage = String()
    var passcountryStr = String()
    var passproductid = Int()
    var passproductaName = String()
    let cellReuseIdentifier = "cell"
    let ingrediantid = String()
    var ingrediantArray = NSArray()
    var selectiontagArray: [String] = []
    var ingredientsQtyArr: [Int] = []
    var IngredientConvertedPrice: [Double] = []
    var quantityNumber = String()
    var productunitprice = Double()
    var getcategoryid = Int()
    var getcategoryName = String()
    var dataenter = String()
    var requiredtickCount = Int()

    

    var counts = [Int](repeating: 0, count: 10)
    var countsmain = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if mustrequired == true {
        reqOneShowLbl.isHidden = true
        }else{
        reqOneShowLbl.isHidden = false

        }
        print(mustrequired)
        
        ingredientstable.register(UINib(nibName: "ingredientTVCellTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
      
        productimage.layer.cornerRadius = 12
        productimage.clipsToBounds = true
        productimage.layer.borderWidth = 1
        productimage.layer.borderColor = UIColor.black.cgColor
        
        tableheightconstant.constant = 5 * 52
       
        setup()
        ingredientlbl.isHidden = true
        ingredientstable.isHidden = true
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        GetIngredientList()
        quantityNumber = "1"
        
        textbaseViewTf.layer.cornerRadius = 8
        textbaseViewTf.layer.borderWidth = 1
        textbaseViewTf.layer.borderColor = UIColor.gray.cgColor
        
        
    }
    
    func setup() {
        
        let url = URL(string: paasprodyctimage )
         
        productimage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        productimage.sd_setImage(with: url) { (image, error, cache, urls) in
              if (error != nil) {
                  // Failed to load image
                self.productimage.image = UIImage(named: "noimage.png")
              } else {
                  // Successful in loading image
                self.productimage.image = image
              }
          }
        
        productunitprice = Double(passunitprice)!
        
        productName.text = passproductaName
       
        let rupee = "$"
        let uprice = passunitprice
        let pri = String(uprice)
        
        let pricedata = pri
        
        productUnitPrice.text = rupee + pricedata
        productSubtotalPrice.text = rupee + pricedata
        
       
    }
    
    @IBAction func addToCartClicked(_ sender: Any) {
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
           
            let alert = UIAlertController(title:nil, message: "Please login to continue. We can't let just anyone have this access to ordering!",         preferredStyle: UIAlertController.Style.alert)

            alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
                
                
                
            }))
            alert.addAction(UIAlertAction(title: "SIGN IN",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            let home = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                            self.navigationController?.pushViewController(home, animated: true)
                                            
            }))
            
            self.present(alert, animated: true, completion: nil)
            alert.view.tintColor = UIColor.black
            
        }else{
            
            if mustrequired == true {
               
                ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                addToCart()
            }else{

            let totalQty = IngredientConvertedPrice.reduce(0, +)
            print("product unit price = \(totalQty)")
                let countOfAndroid = selectiontagArray.filter({ $0 == "YES" }).count
                print("countOfAndroid = \(countOfAndroid)")

            //    if countOfAndroid < 1 || countOfAndroid > 1{
                if countOfAndroid < requiredtickCount {

                   showSimpleAlert(messagess: "Select at least \(requiredtickCount) ingredient")

                }else if countOfAndroid > requiredtickCount {
                    
                    showSimpleAlert(messagess: "Select only \(requiredtickCount) ingredient")

                 }else{

                    ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                    addToCart()

                }


            }
        
       
        }
    }
    
    
    //MARK: - Table View Delegates And Datasource
        
      // number of rows in table view
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
                return ingrediantArray.count

        }
        
        // create a cell for each table view row
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ingredientTVCellTableViewCell
                
                cell.selectionStyle = .none

                
                
            cell.ingredientview.layer.borderWidth = 0.5
            cell.ingredientview.layer.cornerRadius = 6
            cell.ingredientview.layer.borderColor = UIColor.lightGray.cgColor
            
            let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
            
            cell.ingredientname.text!  = dictObj["ingredient_name"] as! String
            let rupee = "$"
            
                let pricedata = dictObj["price"]as! String
            
            if pricedata == "0.00" {
                cell.ingredientview.isHidden = true
                cell.ingredientPrice.isHidden = true
            }else{
                
                cell.ingredientview.isHidden = true
                cell.ingredientPrice.isHidden = false
            }
            
                let conprice = String(pricedata)
            
            cell.ingredientPrice.text = rupee + conprice
        
        cell.minusClicked.tag = indexPath.row
        cell.minusClicked.addTarget(self, action: #selector(MinusClicked(_:)), for: .touchUpInside)
        
        cell.plusClicked.tag = indexPath.row
        cell.plusClicked.addTarget(self, action: #selector(PlusClicked(_:)), for: .touchUpInside)
        
        
        cell.selectwholeingBtn.tag = indexPath.row
        cell.selectwholeingBtn.addTarget(self, action: #selector(selectwholeingBtnClicked(_:)), for: .touchUpInside)
            
//            if selectiontagArray[indexPath.row] == "YES"{
//                cell.selectwholeingBtn.setBackgroundImage(UIImage(named: "right.jpg"), for: UIControl.State.normal)
//
//                let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
//                let pricedata = dictObj["price"]as! String
//
//                IngredientConvertedPrice[indexPath.row] = Double(pricedata)!
//
//                let totalQty = IngredientConvertedPrice.reduce(0, +)
//
//                print("product unit price = \(productunitprice)")
//
//                let addedprice = productunitprice + totalQty
//
//             //   productunitprice = addedprice
//
//                let rupee = "$"
//
//                productSubtotalPrice.text = rupee + String(format: "%.2f", addedprice)
//
//
//                cell.selectwholeingBtn.setBackgroundImage(UIImage(named: "right.jpg"), for: UIControl.State.normal)
//
//             //   selectiontagArray[indexPath.row] = "YES"
//
//              //  print("selectatgarray - \(selectiontagArray)")
//
//                ingredientsQtyArr[indexPath.row] = 1
//                counts[indexPath.row] += 1
//
//            }else{
//
//                cell.selectwholeingBtn.setBackgroundImage(UIImage(named: "blank-square.png"), for: UIControl.State.normal)
//
//
//
//            }
        
        
                return cell
           
        }
    
    
    @IBAction func selectwholeingBtnClicked(_ sender: UIButton) {
    
        if selectiontagArray[sender.tag] == "NO" || selectiontagArray[sender.tag] == "SETTAG"{

            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = ingredientstable.cellForRow(at: indexPath) as! ingredientTVCellTableViewCell

            let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
            let pricedata = dictObj["price"]as! String

            IngredientConvertedPrice[indexPath.row] = Double(pricedata)!
            
            let totalQty = IngredientConvertedPrice.reduce(0, +)

            print("product unit price = \(productunitprice)")

            let addedprice = productunitprice + totalQty

         //   productunitprice = addedprice

            let rupee = "$"

            productSubtotalPrice.text = rupee + String(format: "%.2f", addedprice)
            
          
            cell.selectwholeingBtn.setBackgroundImage(UIImage(named: "right.jpg"), for: UIControl.State.normal)

          //  cell.selctionimg.image = UIImage(named: "right.jpg")

            
            selectiontagArray[indexPath.row] = "YES"

            print("selectatgarray - \(selectiontagArray)")
            
            ingredientsQtyArr[indexPath.row] = 1
            counts[indexPath.row] += 1
            
        }else{

            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = ingredientstable.cellForRow(at: indexPath) as! ingredientTVCellTableViewCell
            
            
            let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
            let pricedata = dictObj["price"]as! String

            counts[indexPath.row] = 0
            IngredientConvertedPrice[indexPath.row] = 0.00
            ingredientsQtyArr[indexPath.row] = 0
            cell.ingredientCountLBL.text = "1"
            
            let totalQty = IngredientConvertedPrice.reduce(0, +)

            print("product unit price = \(productunitprice)")

            let addedprice = productunitprice + totalQty

        //    productunitprice = addedprice

            let rupee = "$"

            productSubtotalPrice.text = rupee + String(format: "%.2f", addedprice)

          //  cell.selctionimg.image = UIImage(named: "blank-square.png")
            cell.selectwholeingBtn.setBackgroundImage(UIImage(named: "blank-square.png"), for: UIControl.State.normal)
     
            selectiontagArray[indexPath.row] = "NO"
            print("selectatgarray - \(selectiontagArray)")

            cell.ingredientPrice.text = rupee + pricedata
            
            
        }

        
    }
    
    
    
    @IBAction func MinusClicked(_ sender: UIButton) {
        
        
        
        if selectiontagArray[sender.tag] == "NO"{
           
           showSimpleAlert(messagess: "Please select ingredient first")
            
        }else{
            
            let indexPath = IndexPath(row: sender.tag, section: 0)
                let cell = ingredientstable.cellForRow(at: indexPath) as! ingredientTVCellTableViewCell
            
            if counts[indexPath.row] <= 1 {
                
            }else{
            
                counts[indexPath.row] -= 1
                
                let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
                let pricedata = dictObj["price"]as! String
                let priceINt = Double(pricedata)!
                let countInt = Double(counts[indexPath.row])
                let sumPP = priceINt * countInt
                cell.ingredientCountLBL.text = "\(counts[indexPath.row])"
                let rupee = "$"
                let convertdouble = String(format: "%.2f", sumPP)
                cell.ingredientPrice.text = rupee + convertdouble
                ingredientsQtyArr[indexPath.row] = counts[indexPath.row]
                IngredientConvertedPrice[indexPath.row] = sumPP
                print("quantityArray - \(ingredientsQtyArr)")
                print("IngredientConvertedPrice - \(IngredientConvertedPrice)")
                
                let totalQty = IngredientConvertedPrice.reduce(0, +)

                print("product unit price = \(productunitprice)")

                let addedprice = productunitprice + totalQty

             //   productunitprice = addedprice

                productSubtotalPrice.text = rupee + String(format: "%.2f", addedprice)
                
            }
            
           
            
           
            
        }
        
    }
    
    @IBAction func PlusClicked(_ sender: UIButton) {
        
        if selectiontagArray[sender.tag] == "NO"{
            
            showSimpleAlert(messagess: "Please select ingredient first")

        }else{
            
            let indexPath = IndexPath(row: sender.tag, section: 0)
                let cell = ingredientstable.cellForRow(at: indexPath) as! ingredientTVCellTableViewCell
                counts[indexPath.row] += 1
            
            let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
            let pricedata = dictObj["price"]as! String
            let priceINt = Double(pricedata)!
            let countInt = Double(counts[indexPath.row])
            let sumPP = priceINt * countInt
            cell.ingredientCountLBL.text = "\(counts[indexPath.row])"
            let rupee = "$"
            let convertdouble = String(format: "%.2f", sumPP)
            cell.ingredientPrice.text = rupee + convertdouble
            
            ingredientsQtyArr[indexPath.row] = counts[indexPath.row]
            IngredientConvertedPrice[indexPath.row] = sumPP
            print("quantityArray - \(ingredientsQtyArr)")
            print("IngredientConvertedPrice - \(IngredientConvertedPrice)")
            
            let totalQty = IngredientConvertedPrice.reduce(0, +)

            print("product unit price = \(productunitprice)")

            let addedprice = productunitprice + totalQty

        //    productunitprice = addedprice

            productSubtotalPrice.text = rupee + String(format: "%.2f", addedprice)
            
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
  
        return 52.0

    }

    //MARK: - tab bar button actions

    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)

    }
    
    @IBAction func locationClicked(_ sender: UIButton) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantListPage") as! RestaurantListPage
        self.navigationController?.pushViewController(home, animated: true)
        
    }
   
    @IBAction func homeClikeched(_ sender: UIButton) {
        
        let order = self.storyboard?.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
        self.navigationController?.pushViewController(order, animated: true)
        
    }
    
    @IBAction func CartClicked(_ sender: UIButton) {
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            showSimpleAlert(messagess: "Please login")
        }else{

        let csrt = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
        self.navigationController?.pushViewController(csrt, animated: true)
        }
    }
    
    @IBAction func orderClicked(_ sender: Any) {
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            showSimpleAlert(messagess: "Please login")
        }else{

        
        let order = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
        self.navigationController?.pushViewController(order, animated: true)
            
        }
        
    }
    
    @IBAction func moreClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
  
    }
    
    
    //MARK: Webservice Call for add to cart

     
    func addToCart() {
        
        
        print(passproductid)
        
        let defaults = UserDefaults.standard
        
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        if defaults.object(forKey: "AvlbCartId") == nil {
          
            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
           
            
        }else{
        
         let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
            let cartidStr = String(avlCartId)
            
       // let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let admintoken = defaults.object(forKey: "custToken")as? String
            
            print("customer token -\(String(describing: admintoken))")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

        let urlString = GlobalClass.DevlopmentApi+"cart-item/"
        
//        let headers: HTTPHeaders = [
//            "Content-Type": "application/json",
//            "Authorization": autho,
//            "user_id": customeridStr,
//            "cart_id": cartidStr,
//            "action": "cart-item"
//        ]

            let now = Date()

                let formatter = DateFormatter()

                formatter.timeZone = TimeZone.current

                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS" ///2020-10-19 12:05:10.625

                let dateString = formatter.string(from: now)
            
            print(dateString)
            var commentStr = String()
            
            if textbaseViewTf.text == nil || textbaseViewTf.text! == "" {
                commentStr = ""
            }else{
                
                commentStr = textbaseViewTf.text! as String
            }
            
           print("commentStr - \(commentStr)")
            
            let cartdatacount = defaults.object(forKey: "cartdatacount")as! String
           // let sendcartcount = cartdatacount
            
            let qtyy = Int(quantityNumber)
            
            let metadataDict = ["updated_at":dateString,"extra":commentStr, "quantity":qtyy! as Int, "cart_id":avlCartId as Any,"product_id":passproductid as Any,"ingredient_id":0,"sequence_id":cartdatacount]
          
            let productarr = [metadataDict]
            print("product element Dict - \(productarr)")
            
            var ingredientarray = Array<Any>()
            
            for (index, selectedIn) in selectiontagArray.enumerated() {
                
                let dictObj = self.ingrediantArray[index] as! NSDictionary
                let ingredientidd = dictObj["ingredient_id"]as! NSNumber
                let idddStr = ingredientidd.stringValue
                let checkingstr = selectedIn
                
                let individualingredientQty = ingredientsQtyArr[index]
                
                if checkingstr == "YES" {
                    
                    let ingredientDict = ["updated_at":dateString,"extra":"test", "quantity":individualingredientQty, "cart_id":avlCartId as Any,"product_id":passproductid as Any,            "ingredient_id":idddStr,"sequence_id":cartdatacount]
                    
                    ingredientarray.append(ingredientDict)
                    
                }
                
            }
        
            
            ingredientarray.insert(metadataDict, at: 0)
            print("Array product with ingredient added - \(ingredientarray)")

        
            let fileUrl = URL(string: urlString)

            var request = URLRequest(url: fileUrl!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(autho, forHTTPHeaderField: "Authorization")
            request.setValue(customeridStr, forHTTPHeaderField: "user_id")
            request.setValue(cartidStr, forHTTPHeaderField: "cart_id")
            request.setValue("cart-item", forHTTPHeaderField: "action")


            request.httpBody = try! JSONSerialization.data(withJSONObject: ingredientarray)

            AF.request(request)
                .responseJSON { response in
                    // do whatever you want here
                    switch response.result {
                                            case .success:
                                                print(response)
                    
                                                if response.response?.statusCode == 201{
                    
                                                    ERProgressHud.sharedInstance.hide()
                    
                                                 let alert = UIAlertController(title: nil, message: "Product added successfully into the cart",         preferredStyle: UIAlertController.Style.alert)
                    
                    
                                                   alert.addAction(UIAlertAction(title: "OK",
                                                                                 style: UIAlertAction.Style.default,
                                                                                 handler: {(_: UIAlertAction!) in
                    
                                                                                    if self.dataenter == "search" {
                                                                                        let cart = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
                                                       
                                                                                         self.navigationController?.pushViewController(cart, animated: false)
                                                                                    }else{
                                                                                    let order = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
                                                   
                                                                                        order.passedcategoryid = self.getcategoryid
                                                   
                                                                                        order.passcategoryname = self.getcategoryName
                                                                                        self.navigationController?.pushViewController(order, animated: false)
                                                                                    }
                    
                                                   }))
                                                   self.present(alert, animated: true, completion: nil)
                                                    alert.view.tintColor = UIColor.black
                    
                                                           
                                                }else{
                    
                                                    if response.response?.statusCode == 401{
                    
                                                        ERProgressHud.sharedInstance.hide()
                                                        self.SessionAlert()
                    
                                                    }else if response.response?.statusCode == 500{
                    
                                                        ERProgressHud.sharedInstance.hide()
                    
                                                        let dict :NSDictionary = response.value! as! NSDictionary
                    
                                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                    }else{
                    
                                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                       }
                    
                                                }
                    
                                                break
                                            case .failure(let error):
                                                ERProgressHud.sharedInstance.hide()
                    
                                                print(error.localizedDescription)
                    
                                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                    
                                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                    
                                                let msgrs = "URLSessionTask failed with error: The request timed out."
                    
                                                if error.localizedDescription == msg {
                    
                                            self.showSimpleAlert(messagess:"No internet connection")
                    
                                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                    
                                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                    
                                                }else{
                    
                                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                                }
                    
                                                   print(error)
                                            }
            }
            
            
        }

}
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: messagess, message: nil,         preferredStyle: UIAlertController.Style.alert)

      //  alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action//
      //  }))
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
        }))
        
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                       
                                        let LoginPage = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(LoginPage, animated: true)
                                        
                                        
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFC4355)
    }
    
    
    
  
    func GetIngredientList(){
        
        var admintoken = String()
        let defaults = UserDefaults.standard
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
       
            admintoken = (defaults.object(forKey: "adminToken")as? String)!
            
        }else{
            
            admintoken = (defaults.object(forKey: "custToken")as? String)!
            
        }
    
        let autho = "token \(admintoken)"
        
        let urlString = GlobalClass.DevlopmentApi+"ingredient/?product_id=\(passproductid)&status=ACTIVE"
        
           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]
      

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                 
                                    let dict :NSDictionary = response.value! as! NSDictionary

                                    let ingredientsList:NSArray = dict.value(forKey: "results")as! NSArray

                                    
                                    if ingredientsList.count == 0 {
                                        
                                        self.ingredientlbl.isHidden = true
                                        self.ingredientstable.isHidden = true
                                        self.ingredientlbl.isHidden = true
                                        self.tableheightconstant.constant = 0
                                        self.ingrediantArray = []
                                        self.selectiontagArray = []
                                        self.ingredientstable.reloadData()
                                        
                                    }else{
                                        
                                       
                                        
                                    for elements in ingredientsList as! [[String:Any]] {


                                        if elements["optional"]as! Bool == true {
                                            let settag = "NO"
                                            let qty = 0
                                            let ingPrice = 0.00
                                            self.selectiontagArray.append(settag)
                                            self.ingredientsQtyArr.append(qty)
                                            self.IngredientConvertedPrice.append(ingPrice)

                                        }else{

                                            let settag = "SETTAG"
                                            let qty = 1
                                            let pricedata = elements["price"]as! String
                                            let priceINt = Double(pricedata)!
                                            let ingPrice = priceINt
                                            self.selectiontagArray.append(settag)
                                            self.ingredientsQtyArr.append(qty)
                                            self.IngredientConvertedPrice.append(ingPrice)

                                        }

                                                

                                        }
                                        
                                        self.requiredtickCount = self.selectiontagArray.reduce(0) { $1 == "SETTAG" ? $0 + 1 : $0 }
                                        
                                        self.ingredientlbl.isHidden = false
                                        
                                        print("selectiontag count - \(self.requiredtickCount)")
                                        
        ///////////////////////////////////////             ///////////////////                 ////////////////////////////////
                                        
                                        print("selectiontag - \(self.selectiontagArray)")
                                        
                                        self.ingredientlbl.isHidden = false
                                        let sun = ingredientsList.count
                                self.tableheightconstant.constant = CGFloat(52 * Int(sun))
                                        
                                        self.ingrediantArray = ingredientsList
                                        self.ingredientstable.isHidden = false
                                        self.ingredientstable.reloadData()
                                        
                                    }
                                    
                                    self.counts = [Int](repeating: 0, count: ingredientsList.count)
                                    
                                    print("ingredientsList = \(ingredientsList)")
                                    
                                    ERProgressHud.sharedInstance.hide()

                                 
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()

                    self.SessionAlert()
                          
                                    
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
  
    //MARK: check cart Section
    
    //MARK: check cart Api
       
        func checkCartAvl()  {
           
           let defaults = UserDefaults.standard
           
           let savedUserData = defaults.object(forKey: "custToken")as? String
           
           let customerid = defaults.integer(forKey: "custId")
           let custidStr = String(customerid)
           
           let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
               
           let urlString = GlobalClass.DevlopmentApi+"cart/?customer_id="+custidStr+""
           print("Url cust avl - \(urlString)")
               
               let headers: HTTPHeaders = [
                   "Content-Type": "application/json",
                   "Authorization": token,
                   "user_id": custidStr,
                   "action": "cart"
               ]

                AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
               response in
                 switch response.result {
                               case .success:
                                   
                                 //  print(response)

                                   if response.response?.statusCode == 200{
                                     //  self.dissmiss()
                                       
                                       let dict :NSDictionary = response.value! as! NSDictionary
                                                    //  print(dict)
                                       
                                       let status = dict.value(forKey: "results")as! NSArray
                                                      print(status)
                                                        
                                           print("store available or not - \(status)")
                                       
                                       
                                                        
                   if status.count == 0 {
                       
                  //  ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                       self.createCart()
                                                           
                   }else{
                            
                               let firstobj:NSDictionary  = status.object(at: 0) as! NSDictionary

                               let getAvbcartId = firstobj["id"] as! Int
                        let storeWRTCart = firstobj["restaurant"] as! Int


                        let defaults = UserDefaults.standard

                               defaults.set(getAvbcartId, forKey: "AvlbCartId")
                               defaults.set(storeWRTCart, forKey: "storeIdWRTCart")


                                    print("avl cart id - \(getAvbcartId)")
                                    print("cart w R to store  - \(storeWRTCart)")


                    self.GetIngredientList()
                       
                 //   ERProgressHud.sharedInstance.hide()

               }
                     
               }else{
                                       
                  // self.dissmiss()
                   print(response)
                   if response.response?.statusCode == 401{
                                          
                       self.SessionAlert()
                                           
                    }else if response.response?.statusCode == 500{
                                                                      
                 //  self.dissmiss()
                                                                      
                                                                       let dict :NSDictionary = response.value! as! NSDictionary
                                                                      
                                                                      self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                                  }else{
                                                                   
                                                                   self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                                  }
               }
                                   
                                   break
                               case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
               }
               
         
               
           }
       
       
    
    func createCart() {
        
        
        let defaults = UserDefaults.standard
        
        let savedUserData = defaults.object(forKey: "custToken")as? String
         let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
       
        let customerid = defaults.integer(forKey: "custId")
        let custidStr = String(customerid)
       

//        let urlString = GlobalObjects.DevlopmentApi+"cart/?customer_id=\(customerid)&restaurant_id=\(passedrestaurantid)"

        let urlString = GlobalClass.DevlopmentApi+"cart/"
        
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": token,
            "user_id": custidStr,
            "action": "cart"
        ]
        
        AF.request(urlString, method: .post, parameters: ["customer_id":customerid,"restaurant":GlobalClass.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                                                 //   self.dissmiss()
                                                    
                                                    let resultarr :NSDictionary = response.value! as! NSDictionary
                                
                                                print(resultarr)
                                
                                
                             //   let resultarr : NSArray = dict.value(forKey: "results") as! NSArray
                                
                                if resultarr.count == 0 {
                                  
                                    self.showSimpleAlert(messagess: "Cart not created")
                                    
                                }else{
                                
                             //   let dictObj = resultarr[0] as! NSDictionary

                                                    
                                                    let getAvbcartId = resultarr["id"] as! Int
                                                                                                      let storeWRTCart = resultarr["restaurant"] as! Int
                                                                                                      
                                                                                                      let defaults = UserDefaults.standard
                                                                                                      
                                                                                                      defaults.set(getAvbcartId, forKey: "AvlbCartId")
                                                                                                      defaults.set(storeWRTCart, forKey: "storeIdWRTCart")
                                                                                                           
                                                                                                           print("avl cart id - \(getAvbcartId)")
                                                                                                           print("cart w R to store  - \(storeWRTCart)")
                                                                     
                            //    self.dissmiss()
                                  
                       //     self.performSegue(withIdentifier: "productListVC", sender: self)
                                    
                                }
                                
                            //    ERProgressHud.sharedInstance.hide()

                                self.GetIngredientList()
                                
                            }else{
                             
                              if response.response?.statusCode == 500{
                                                            
                                   //                         self.dissmiss()
                                                            
                                                             let dict :NSDictionary = response.value! as! NSDictionary
                                                            
                                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                        }else{
                                                            
                                                      //      self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                           }
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }


     }
    
    
    // MARK: UIStepperControllerDelegate functions

    @IBAction func stepperDidAddValues(_ sender: UIButton) {
    
       countsmain = (countsmain + 1)
//        let countprice = Double(countsmain) * productunitprice
//        let rupee = "$"
//        productSubtotalPrice.text = rupee + String(format: "%.2f", countprice)
        
        
        let qut = countsmain
        quantityNumber = String(qut)
      //  let first = Double(passunitprice)
        plusminusCount.text = quantityNumber
        let first = Double(passunitprice)
        let second = Double(countsmain)
        
        var sum = Double()
        sum = first! * second
        
        productunitprice = sum
        
        let totalQty = IngredientConvertedPrice.reduce(0, +)
        
        let withingsum = totalQty + sum
        
     //   productSubtotalPrice.text = String(format: "%.2f", sum)
       
            let rupee = "$"
            
            productSubtotalPrice.text = rupee + String(format: "%.2f", withingsum)
        
        
    }
    
    @IBAction func stepperDidSubtractValues(_ sender: UIButton) {
        
        if countsmain <= 1 {
            
        }else{
            
        countsmain = (countsmain - 1)
//        let countprice = Double(countsmain) * productunitprice
//        let rupee = "$"
//        productSubtotalPrice.text = rupee + String(format: "%.2f", countprice)
            
            let qut = countsmain
            quantityNumber = String(qut)
          //  let first = Double(passunitprice)
            plusminusCount.text = quantityNumber
            let first = Double(passunitprice)
            let second = Double(countsmain)
            
            var sum = Double()
            sum = first! * second
            
            productunitprice = sum
            
            let totalQty = IngredientConvertedPrice.reduce(0, +)
            
            let withingsum = totalQty + sum
            
         //   productSubtotalPrice.text = String(format: "%.2f", sum)
           
                let rupee = "$"
                
                productSubtotalPrice.text = rupee + String(format: "%.2f", withingsum)
            
            
            
        }
        
    }
    

}
