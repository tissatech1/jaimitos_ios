//
//  ContactUsPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/29/21.
//

import UIKit
import Alamofire

class ContactUsPage: UIViewController {
    @IBOutlet weak var phTF: UILabel!
    @IBOutlet weak var emailTF: UILabel!
    @IBOutlet weak var addressTF: UILabel!
    @IBOutlet weak var timeTF: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        getrestaurantApi()
    }
    

    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)

    }
    
    func getrestaurantApi(){
        
        var admintoken = String()
        let defaults = UserDefaults.standard
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            admintoken = (defaults.object(forKey: "adminToken")as? String)!
        }else{
            admintoken = (defaults.object(forKey: "custToken")as? String)!
        }
        
        let autho = "token \(admintoken)"
        let urlString = GlobalClass.DevlopmentApi+"restaurant/\(GlobalClass.restaurantGlobalid)/"

           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]
      

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                    
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                   
                                   let dataPH  = dict["phone"]as? String
                                    let dataEmail  = dict["email"]as? String
                                    let dataADD  = dict["address"]as! String
                                    let dataCITY  = dict["city"]as! String
                                    let dataState  = dict["state"]as! String
                                    let dataZip  = dict["zip"]as! String
                                    let dataTime  = dict["working_hours"]as? String

                                    self.phTF.text! = dataPH ?? "(210)5248161"
                                    self.emailTF.text! = dataEmail ?? "maduraimes@gmail.com"
                                    self.addressTF.text! = "\(dataADD) \(dataCITY), \(dataState) \(dataZip)"
                                    
                                    self.timeTF.text! = dataTime ?? "Monday - Friday : 9 am to 5 pm Saturday and Sunday : Closed"
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()

                    self.SessionAlert()
                          
                                    
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No internet connection")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                                }else{
                                
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                       

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                      

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let LoginPage = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(LoginPage, animated: true)
                                        
                                        
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    

}
