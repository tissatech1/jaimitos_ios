//
//  AfterpaymentPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/27/21.
//

import UIKit
import Alamofire
import SDWebImage
import SideMenu
class AfterpaymentPage: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var cardview: UIScrollView!
    @IBOutlet weak var cardview2: UIView!
    @IBOutlet weak var producttable: UITableView!
    @IBOutlet weak var tableheightconstant: NSLayoutConstraint!
    
    @IBOutlet weak var orderNoLbl: UILabel!
    @IBOutlet weak var orderstorename: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var subtotalLbl: UILabel!
    @IBOutlet weak var tiplbl: UILabel!
    @IBOutlet weak var serviceFeeLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var shippingFeeLbl: UILabel!
    @IBOutlet weak var discountlbl: UILabel!
    @IBOutlet weak var orderTotalLbl: UILabel!
    @IBOutlet weak var addline1Lbl: UILabel!
    @IBOutlet weak var addline2Lbl: UILabel!
    @IBOutlet weak var addline3Lbl: UILabel!
    @IBOutlet weak var addline4Lbl: UILabel!
    @IBOutlet weak var addline5Lbl: UILabel!
    @IBOutlet weak var addline6Lbl: UILabel!
    @IBOutlet weak var backHomeBtn: UIButton!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var addressviewheight: NSLayoutConstraint!
    @IBOutlet weak var detailtopurchsaseverticleSpace: NSLayoutConstraint!
    @IBOutlet weak var addressboldTitle: UILabel!
    @IBOutlet weak var addressblockTitle: UILabel!
    @IBOutlet weak var paymentMethodtitle: UILabel!
    @IBOutlet weak var shippingmethodTitle: UILabel!
    
    var  orderIDGet = Int()
    var fetchedItems = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        producttable.register(UINib(nibName: "orderDelCell", bundle: nil), forCellReuseIdentifier: "Cell")
                
        producttable.rowHeight = UITableView.automaticDimension
        producttable.estimatedRowHeight = 84
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        setdata()
         
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
         orderitems()
        orderstorename.text = " CHOCOLATE TALES"
        
    }
    
    //MARK: - Table View Delegates And Datasource
    
  // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fetchedItems.count
    
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! orderDelCell

        cell.selectionStyle = .none
        
        cell.tableCellOuter.layer.cornerRadius = 8
        cell.tableCellOuter.layer.shadowColor = UIColor.lightGray.cgColor
        cell.tableCellOuter.layer.shadowOpacity = 1
        cell.tableCellOuter.layer.shadowOffset = .zero
        cell.tableCellOuter.layer.shadowRadius = 3
        
        if fetchedItems.count == 0 {
            
        }else{
        
        
         let dictObj = self.fetchedItems[indexPath.row] as! NSDictionary

        cell.cartProName.text = dictObj["product_name"] as? String

            let rupee = "$"

            let pricedata = dictObj["line_total"]as! String

           cell.costLbl.text = rupee + pricedata


        var urlStr = String()

        if dictObj["product_url"] is NSNull {
            urlStr = ""
        }else{

            urlStr = dictObj["product_url"] as! String

        }

            let qnt = dictObj["quantity"] as! Int
            cell.quantityLbl.text = "Qty : " + String(qnt)

        let url = URL(string: urlStr )

       cell.cartProductImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.cartProductImg.sd_setImage(with: url) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                cell.cartProductImg.image = UIImage(named: "noimage.png")
            } else {
                // Successful in loading image
                cell.cartProductImg.image = image
            }
        }
            
            let list:NSArray = dictObj.value(forKey: "ingredient") as! NSArray

            if list.count == 0 {

                cell.ingredients.isHidden = true

            }else{
                cell.ingredients.isHidden = false

                var indnamearr:[String] = []
                var indpricearr:[Double] = []
                var indpricearrforshow:[String] = []

                for (index, element1) in list.enumerated() {

                    let dictObj1 = list[index] as! NSDictionary
                    let ingredprice = dictObj1["line_total"]as! String
                    let ingredientname = dictObj1["ingredient_name"]as! String
                    let quantitystrrr = dictObj1["quantity"]as! Int
                    let convertqty = Int(quantitystrrr)

                    let nameshow = "\(index+1).\(ingredientname):\nPrice :$\(ingredprice)\nQty:\(convertqty)\n\n"

                    indpricearrforshow.append(nameshow)
                    indnamearr.append(ingredientname)
                    indpricearr.append(Double(ingredprice)!)
                }

                let total = indpricearr.reduce(0, +)

                print("ingredient total - \(total)")

                let rupee = "$"

                           let pricedata = dictObj["line_total"]as! String

                           let Pamt = Double(pricedata)

                           let Tamt =  Double(Pamt!) + total


                          cell.costLbl.text = rupee + String(format: "%.2f", Tamt)

        cell.ingredients.text = indpricearrforshow.map { String($0) }.joined(separator: "")

            }
          
        }

        return cell
    }
    

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        
            return UITableView.automaticDimension
        
    }
    
    func setdata() {
        let defaultns = UserDefaults.standard
        let order:NSDictionary = defaultns.object(forKey: "passOrderResult")as! NSDictionary

        print("orderInfo dict - \(order)")
        
        let numbersh = order["order_id"]as! Int
        
        self.orderNoLbl.text! = String(numbersh)

        let strdate = order["created_at"]as! String
        
      
        
        let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
                dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                let date = dateFormatter.date(from: strdate)// create   date from string

                // change to a readable time format and change to local time zone
                dateFormatter.dateFormat = "MMMM dd, yyyy"
                dateFormatter.timeZone = NSTimeZone.local
                let timeStamp = dateFormatter.string(from: date!)
        
        
        self.orderDateLbl.text! = timeStamp
        self.subtotalLbl.text! = "$\(order["subtotal"]as! String)"
        self.tiplbl.text! = "$\(order["tip"]as! String)"
      //  self.serviceFeeLbl.text! = "$\(order["service_fee"]as! String)"
        self.taxLbl.text! = "$\(order["tax"]as! String)"
      //  self.shippingFeeLbl.text! = "$\(order["shipping_fee"]as! String)"
        self.discountlbl.text! = "$\(order["discount"]as! String)"
        self.orderTotalLbl.text! = "$\(order["total"]as! String)"
        
       
        
                    let defaults = UserDefaults.standard
                    let billing:NSDictionary = defaults.object(forKey: "billingaddressDICT")as! NSDictionary
        
                    print("billing dict - \(billing)")
        
        let one = billing["house_number"]as! String
              let two = billing["address"]as! String
        
                self.addline1Lbl.text! = billing["name"]as! String
                self.addline2Lbl.text! = two + " " + one
                self.addline3Lbl.text! = billing["city"]as! String
                self.addline4Lbl.text! = billing["country"]as! String
                self.addline5Lbl.text! = billing["zip"]as! String
                self.addline6Lbl.text! = ""
                
                
        
        
    }
   
    func orderitems()  {
        
        let defaults = UserDefaults.standard
        
      //  let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let admintoken = defaults.object(forKey: "custToken")as? String
           
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalClass.DevlopmentApi+"order-item/?order_id=\(orderIDGet)"
        
       

            
        print("oderitems get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                
                                    let dict1 :NSDictionary = response.value! as! NSDictionary
                                     
                                   self.fetchedItems = (dict1.value(forKey:"results")as! NSArray)
                                      // print(self.productList)
                                         if self.fetchedItems.count == 0 {
                                           
                                       
                                            
                                            self.producttable.reloadData()
                                            
                                            ERProgressHud.sharedInstance.hide()
                                         }else{
                                            
                                           // let sun = self.fetchedItems.count
                                            
                                            
                                            tableheightconstant.constant = producttable.estimatedRowHeight + 100 * CGFloat(fetchedItems.count)
                                            
                                            self.producttable.reloadData()
                                            
                                            ERProgressHud.sharedInstance.hide()

                                         }
                    
                                 
                                }else{
                                    
                        if response.response?.statusCode == 401{
                                    
                            ERProgressHud.sharedInstance.hide()
                        self.SessionAlert()
                          
                           }else if response.response?.statusCode == 500{
                                        
                            ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                           }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let home = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(home, animated: true)
                                        
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
   
    
   
    //MARK: - tab bar button actions

    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
        self.navigationController?.pushViewController(home, animated: true)
        
    }

}
