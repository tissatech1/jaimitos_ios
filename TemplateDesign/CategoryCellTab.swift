//
//  CategoryCellTab.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 6/18/21.
//

import UIKit

class CategoryCellTab: UITableViewCell {

    @IBOutlet weak var outerview: UIView!
    @IBOutlet weak var catename: UILabel!
    @IBOutlet weak var categoryImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
