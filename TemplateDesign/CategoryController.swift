//
//  CategoryController.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 5/7/21.
//

import UIKit
import SideMenu
import SDWebImage
import Alamofire
import SimpleCollapsingHeaderView

class CategoryController: UIViewController {
    @IBOutlet weak var categoryview: UITableView!
    @IBOutlet weak var cartcountLbl: UILabel!
    
    var TitlesList = NSArray()
    var restStatusStr = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        categoryview.register(UINib(nibName: "CategoryCellTab", bundle: nil), forCellReuseIdentifier: "Cell")
        
        categoryview.register(UINib(nibName: "CategorynewCell", bundle: nil), forCellReuseIdentifier: "celln")
        
       // cartcountLbl.layer.cornerRadius = 6
        cartcountLbl.layer.borderWidth = 1
        cartcountLbl.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        cartcountLbl.text = "0"

        categoryview.rowHeight = UITableView.automaticDimension
        categoryview.estimatedRowHeight = 86
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
        
        adminlogincheck()
            
        }else{
        checkCartAvl()
        gettiming()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
           
        }else{
            
            cartcountApi()
        }
        
    }
    
    //MARK: Admin Login
    
    func adminlogincheck(){

        let urlString = GlobalClass.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalClass.adminusername, "password":GlobalClass.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "adminToken")
                            self.gettiming()
                          //  self.GetCategoryList()
                          
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
    
    //MARK: - tab bar button actions
    @IBAction func cartClicked(_ sender: UIButton) {
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            
            showSimpleAlert(messagess: "Please login")
            
        }else{
        
        if restStatusStr == "open" {
        
        let csrt = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
        self.navigationController?.pushViewController(csrt, animated: true)
            
        }else{
            
            showSimpleAlert(messagess: "Restaurant is closed")
            
        }
        }
    }
    
    @IBAction func searchClicked(_ sender: Any) {
       
        let order = self.storyboard?.instantiateViewController(withIdentifier: "SearchPage") as! SearchPage
        self.navigationController?.pushViewController(order, animated: true)
        
    }
    
    @IBAction func moreClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
        
    }
    
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    


}

// MARK: - UICollectionViewDataSource
extension CategoryController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return TitlesList.count
        
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CategoryCellTab

        cell.selectionStyle = .none
        let dictObj = self.TitlesList[indexPath.row] as! NSDictionary

        cell.catename.text = dictObj["category"] as? String

        cell.catename.layer.cornerRadius = 8
        cell.catename.layer.borderWidth = 0.5
        cell.catename.layer.borderColor = UIColor(rgb: 0x40CF36).cgColor
//        cell.catename.layer.shadowColor =  UIColor(rgb: 0x466A37).cgColor
//        cell.catename.layer.shadowOpacity = 1
//        cell.catename.layer.shadowOffset = .zero
//        cell.catename.layer.shadowRadius = 5

        var urlStr = String()
        if dictObj["category_url"] is NSNull || dictObj["category_url"] == nil{

            urlStr = ""

        }else{
            urlStr = dictObj["category_url"] as! String
        }

        let url = URL(string: urlStr )

        cell.categoryImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.categoryImg.sd_setImage(with: url) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                cell.categoryImg.image = UIImage(named: "noimage.png")
            } else {
                // Successful in loading image
                cell.categoryImg.image = image
            }
        }

        return cell
        }else{
            let celln = tableView.dequeueReusableCell(withIdentifier: "celln", for: indexPath) as! CategorynewCell
            
            celln.selectionStyle = .none
            let dictObj = self.TitlesList[indexPath.row] as! NSDictionary
            
            celln.catLbl.text = dictObj["category"] as? String
            
            celln.catLbl.layer.cornerRadius = 8
            celln.catLbl.layer.borderWidth = 0.5
            celln.catLbl.layer.borderColor = UIColor(rgb: 0x40CF36).cgColor
    //        cell.outerview.layer.shadowColor =  UIColor(rgb: 0x466A37).cgColor
    //        cell.outerview.layer.shadowOpacity = 1
    //        cell.outerview.layer.shadowOffset = .zero
    //        cell.outerview.layer.shadowRadius = 5
            
            var urlStr = String()
            if dictObj["category_url"] is NSNull || dictObj["category_url"] == nil{

                urlStr = ""

            }else{
                urlStr = dictObj["category_url"] as! String
            }
            
            let url = URL(string: urlStr )

            celln.catimage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            celln.catimage.sd_setImage(with: url) { (image, error, cache, urls) in
                if (error != nil) {
                    // Failed to load image
                    celln.catimage.image = UIImage(named: "noimage.png")
                } else {
                    // Successful in loading image
                    celln.catimage.image = image
                }
            }
            
            return celln
            }
        
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
       
        return UITableView.automaticDimension
     
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    let home = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
    let dictObj = self.TitlesList[indexPath.item] as! NSDictionary
    let categoryid = dictObj["category_id"]as! Int
    home.passedcategoryid =  categoryid
        let categoryname  = dictObj["category"]as! String
    home.passcategoryname = categoryname
    self.navigationController?.pushViewController(home, animated: true)
    
    }
}

// MARK: - CollectionViewWaterfallLayoutDelegate

extension CategoryController: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
        self.view.alpha = 0.5
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Appeared! (animated: \(animated))")
        
        self.view.alpha = 0.5
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappearing! (animated: \(animated))")
        
        self.view.alpha = 1
        
       // self.performSegue(withIdentifier: "login", sender: self)
    }
    
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappeared! (animated: \(animated))")
        
        self.view.alpha = 1
        
      
    }
    
    
    //MARK: Webservice Call category
        
        
        func GetCategoryList(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
           
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
                
            }else{
                
                admintoken = (defaults.object(forKey: "custToken")as? String)!
                
            }
            
            let autho = "token \(admintoken)"
            
            let urlString = GlobalClass.DevlopmentApi+"category/?restaurant_id=\(GlobalClass.restaurantGlobalid)"
            
               
            print(" categoryurl - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                    print(response)

                                    if response.response?.statusCode == 200{
                                     
                                     let dict :NSArray = response.value! as! NSArray
                                     
                                        if dict.count == 0 {
                                            self.TitlesList = []
                                            self.categoryview.reloadData()
                                            
                                        }else{
                                            
                                            
                                          
                                            self.TitlesList = dict
                                            
//                    let dictObjcat = self.TitlesList[0] as! NSDictionary
//                    let categoryiddd = dictObjcat["category_id"]
//                    self.passedcategoryid = categoryiddd as! Int
//                    self.passcategoryname = dictObjcat["category"]as! String
                                          
                                            self.categoryview.reloadData()
                                            
                                        }
                                        
                                        ERProgressHud.sharedInstance.hide()

                                     
                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {

                                self.showSimpleAlert(messagess:"No internet connection")

                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                                self.showSimpleAlert(messagess:"Slow Internet Detected")

                                    }else{
                                    
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                }
                }
                
          
                
            }
        
  
    
    //MARK: Webservice Call timing
        
        
        func gettiming(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
            }else{
                admintoken = (defaults.object(forKey: "custToken")as? String)!
            }
            
            let autho = "token \(admintoken)"
            
            let urlString = GlobalClass.DevlopmentApi+"hour/?restaurant_id=\(GlobalClass.restaurantGlobalid)"
            
               
            print(" categoryurl - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                    print(response)

                                    if response.response?.statusCode == 200{
                                        
                                        let dict :NSDictionary = response.value! as! NSDictionary
                                       
                                        let data  = dict["status"]as! String
                                        
                                        self.restStatusStr = data
                                        
                                      //  self.restStatusStr = "open"
                                        
                                       // data = "closed"
                                        
                                        if data == "closed" {
                                         
                                            print("restaurant is - \(data)")
                                            
                                          //  self.categoryshNameLbl.text = "Closed"
                                          
                                          //  self.categoryview.alpha = 0.5
                                            
                                        }else{
                                        
                                            
                                            print("restaurant is - open")
                                        }
                                        
                                        self.GetCategoryList()
                                        
                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {

                                self.showSimpleAlert(messagess:"No internet connection")

                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                                self.showSimpleAlert(messagess:"Slow Internet Detected")

                                    }else{
                                    
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                    
                                    
                                    
                                }
                }
                
          
                
            }
        
    
    //MARK: check cart Section
    
    //MARK: check cart Api
       
        func checkCartAvl()  {
           
           let defaults = UserDefaults.standard
           
           let savedUserData = defaults.object(forKey: "custToken")as? String
           
           let customerid = defaults.integer(forKey: "custId")
           let custidStr = String(customerid)
           
           let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
               
            let urlString = GlobalClass.DevlopmentApi+"cart/?customer_id="+custidStr+"&restaurant=" + GlobalClass.restaurantGlobalid + ""
           print("Url cust avl - \(urlString)")
               
               let headers: HTTPHeaders = [
                   "Content-Type": "application/json",
                   "Authorization": token,
                   "user_id": custidStr,
                   "action": "cart"
               ]

                AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
               response in
                 switch response.result {
                               case .success:
                                   
                                 //  print(response)

                                   if response.response?.statusCode == 200{
                                     //  self.dissmiss()
                                       
                                       let dict :NSDictionary = response.value! as! NSDictionary
                                                    //  print(dict)
                                       
                                       let status = dict.value(forKey: "results")as! NSArray
                                                      print(status)
                                                        
                                           print("store available or not - \(status)")
                                       
                                       
                                                        
                   if status.count == 0 {
                       
                  //  ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                       self.createCart()
                                                           
                   }else{
                            
                               let firstobj:NSDictionary  = status.object(at: 0) as! NSDictionary

                               let getAvbcartId = firstobj["id"] as! Int
                        let storeWRTCart = firstobj["restaurant"] as! Int


                        let defaults = UserDefaults.standard

                               defaults.set(getAvbcartId, forKey: "AvlbCartId")
                               defaults.set(storeWRTCart, forKey: "storeIdWRTCart")


                                    print("avl cart id - \(getAvbcartId)")
                                    print("cart w R to store  - \(storeWRTCart)")


                    self.cartcountApi()
                       
                 //   ERProgressHud.sharedInstance.hide()

               }
                     
               }else{
                                       
                  // self.dissmiss()
                   print(response)
                   if response.response?.statusCode == 401{
                                          
                       self.SessionAlert()
                                           
                    }else if response.response?.statusCode == 500{
                                                                      
                 //  self.dissmiss()
                                                                      
                                                                       let dict :NSDictionary = response.value! as! NSDictionary
                                                                      
                                                                      self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                                  }else{
                                                                   
                                                                   self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                                  }
               }
                                   
                                   break
                               case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No internet connection")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                                }else{
                                
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    
                                }

                                   print(error)
                            }
               }
               
         
               
           }
       
       
    
    func createCart() {
        
        
        let defaults = UserDefaults.standard
        
        let savedUserData = defaults.object(forKey: "custToken")as? String
         let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
       
        let customerid = defaults.integer(forKey: "custId")
        let custidStr = String(customerid)
       

//        let urlString = GlobalObjects.DevlopmentApi+"cart/?customer_id=\(customerid)&restaurant_id=\(passedrestaurantid)"

        let urlString = GlobalClass.DevlopmentApi+"cart/"
        
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": token,
            "user_id": custidStr,
            "action": "cart"
        ]
        
        AF.request(urlString, method: .post, parameters: ["customer_id":customerid,"restaurant":GlobalClass.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                                                 //   self.dissmiss()
                                                    
                                                    let resultarr :NSDictionary = response.value! as! NSDictionary
                                
                                                print(resultarr)
                                
                                
                             //   let resultarr : NSArray = dict.value(forKey: "results") as! NSArray
                                
                                if resultarr.count == 0 {
                                  
                                    self.showSimpleAlert(messagess: "Cart not created")
                                    
                                }else{
                                
                             //   let dictObj = resultarr[0] as! NSDictionary

                                                    
                                                    let getAvbcartId = resultarr["id"] as! Int
                                                                                                      let storeWRTCart = resultarr["restaurant"] as! Int
                                                                                                      
                                                                                                      let defaults = UserDefaults.standard
                                                                                                      
                                                                                                      defaults.set(getAvbcartId, forKey: "AvlbCartId")
                                                                                                      defaults.set(storeWRTCart, forKey: "storeIdWRTCart")
                                                                                                           
                                                                                                           print("avl cart id - \(getAvbcartId)")
                                                                                                           print("cart w R to store  - \(storeWRTCart)")
                                                  
                                    self.cartcountApi()
                                    
                            //    self.dissmiss()
                                  
                       //     self.performSegue(withIdentifier: "productListVC", sender: self)
                                    
                                }
                                
                            //    ERProgressHud.sharedInstance.hide()

                                
                            }else{
                             
                              if response.response?.statusCode == 500{
                                                            
                                   //                         self.dissmiss()
                                                            
                                                             let dict :NSDictionary = response.value! as! NSDictionary
                                                       
                                let message  = dict["message"]as! String
                           let substr = "duplicate key value violates unique constraint"
                                
                                if message.contains(substr) {
                                    
                                    print("I found: \(message)")
                                }else{
                                    
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    
                                }
                                                        }else{
                                                            
                                                      //      self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                           }
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }


     }
    
  
    func cartcountApi()
   {

      let defaults = UserDefaults.standard
      let customerid = defaults.integer(forKey: "custId")

        //  let customerid = 16
        
    let urlString = GlobalClass.DevlopmentGraphql


 //   let stringempty = "query{cartItemCount(token:\"\(GlobalObjects.globGraphQlToken)\", customerId :\(customerid)){\n count\n }\n}"

        let restidd = GlobalClass.restaurantGlobalid
       
        
        let stringempty = "query{cartItemCount(token:\"\(GlobalClass.globGraphQlToken)\",customerId :\(customerid),restaurantId:\(restidd)){\n count\n }\n}"
        
            AF.request(urlString, method: .post, parameters: ["query": stringempty],encoding: JSONEncoding.default, headers: nil).responseJSON {
           response in
             switch response.result {
                           case .success:
                              // print(response)

                               if response.response?.statusCode == 200{

                              let dict :NSDictionary = response.value! as! NSDictionary
                             // print(dict)
                                
                                if dict.count == 2 {

                                   // self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                    self.cartcountLbl.text = "0"
                                    
                                    let countStr = self.cartcountLbl.text
                                    let defaults = UserDefaults.standard
                                    defaults.set(countStr, forKey: "cartdatacount")
                                    
                                }else{
                                
                                
                                
                                let status = dict.value(forKey: "data")as! NSDictionary
                              print(status)


                                let newdict = status.value(forKey: "cartItemCount")as! NSDictionary

                                let num = newdict["count"] as! Int

                                self.cartcountLbl.text = String(num)

                                    let countStr = self.cartcountLbl.text
                                    let defaults = UserDefaults.standard
                                    defaults.set(countStr, forKey: "cartdatacount")
                                    
                                }
                              //  self.dissmiss()

                               }else{

                                
                                if response.response?.statusCode == 500{
                                    
                              //      ERProgressHud.sharedInstance.hide()

                              //      let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                 //   self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else if response.response?.statusCode == 401{
                                    ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                    
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                                


                               }

                               break
                           case .failure(let error):
                            
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow internet detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
           }


        }

    
   
    
    
}


